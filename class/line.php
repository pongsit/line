<?php

namespace pongsit\line;

class line extends \pongsit\model\model{
	public function __construct(){
		parent::__construct();
	}
	function get_the_info($userId){
		$query = "SELECT * FROM ".$this->table." where userId='$userId';";
		$outputs = $this->db->query_array0($query);
		return $outputs;
	}
	function get_user_id($line_id){
		$query = "SELECT user_id FROM line_user where line_id='$line_id';";
		$outputs = $this->db->query_array0($query);
		return $outputs['user_id'];
	}
	function count_me(){
		$results = $this->db->query_array0("select count(distinct userId) as count from ".$this->table);
		return $results['count'];
	}
	function search_for_user($s){
		$query = "SELECT * FROM ".$this->table." WHERE displayName like '%$s%' LIMIT 10;";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	function get_info_from_user_id($user_id){
		$query = "SELECT line_id FROM line_user where user_id='$user_id';";
		$outputs = $this->db->query_array0($query);
		if(!empty($outputs['line_id'])){
			return $this->get_info($outputs['line_id']);
		}else{
			return array();
		}
	}
	function login($userId){
		$auth = new \pongsit\auth\auth();
		$user = new \pongsit\user\user();
		$url = new \pongsit\url\url();
		$line_user_infos = $this->get_the_info($userId);
		$user_id = $this->get_user_id($line_user_infos['id']);
		if(empty($user_id)){
			return false;
		}
		$user_infos = $user->get_info($user_id);
		if(!$user_infos['active']){
			if($user_id!=1){
				return false;
			}
		}
		$urls = $url->get_current('array');
		$auth->add_session(array('user','id'),$user_infos['id']);
		$auth->add_session(array('user','username'),$user_infos['name']);
		$auth->add_session(array('user','name'),$user_infos['name']);
		$auth->add_session(array('user','baseurl'), $GLOBALS['baseurl']);
		
		if(!empty($line_user_infos['pictureUrl']) && !file_exists($GLOBALS['path_to_app'].'system/img/profile/'.$user_id)){
			copy($line_user_infos['pictureUrl'], $GLOBALS['path_to_app'].'system/img/profile/'.$user_id);
		}
		
		return true;	
	}

	function insert_user($line_id,$user_id){
		return $this->db->insert('line_user',array('user_id'=>$user_id,'line_id'=>$line_id));
	}
}