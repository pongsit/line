CREATE TABLE `line` (
  `id` INT AUTO_INCREMENT primary key NOT NULL,
  `userId` text NOT NULL,
  `displayName` text NOT NULL,
  `pictureUrl` text,
  `active` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_line` (
  `user_id` int(11) NOT NULL,
  `line_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

