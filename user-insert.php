<?php
	
require_once("../system/init.php");

// $option = new \pongsit\view\option();
$role = new \pongsit\role\role();
$user = new \pongsit\user\user();
$line = new \pongsit\line\line();

if(empty($_GET['userId'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create();
	exit();
}

$line_user_infos = $line->get_the_info($_GET['userId']);
if(empty($line_user_infos['id'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create();
	exit();
}
$line_id = $line_user_infos['id'];
$displayName = $line_user_infos['displayName'];

if(!empty($line->get_user_id($line_id))){
	if($line->login($_GET['userId'])){
		header('Location: '.$path_to_core.'user/info.php?id='.$latest_id);
		exit();
	}else{
		$view = new \pongsit\view\view('locked');
		echo $view->create();
		exit();
	}
}

$notification = '';
if(!empty($_POST)){
	if( !empty($_POST['username']) && 
		!empty($_POST['password']) &&
		!empty($_POST['password_retype']) &&
		!empty($line_id)
	){
		unset($_POST['submit']);
		if($_POST['password']==$_POST['password_retype']){
			if(!$user->exist($_POST['username'])){
				$this_user_id = $line->get_user_id($line_id);
				if(!file_exists($path_to_root.'app/system/img/profile')){
					mkdir($path_to_root.'app/system/img/profile',0744,true);
				}
				if(empty($this_user_id)){
					$hash = $auth->generatePassword($_POST['password']);
					$latest_id = $user->insert(array('name'=>$_POST['username'],'password'=>$hash));
					// $user_id = $user->get_last_inserted_id();
					$line->insert_user($line_id,$latest_id);
					if(!($_SESSION['user']['id']==1 || $role->check('admin'))){
						if($line->login($_GET['userId'])){
							// $notification = $view->block('alert',array('message'=>'เพิ่มผู้ใช้เรียบร้อย','type'=>'success','css'=>'col-7'));
							header('Location: '.$path_to_core.'user/info.php?id='.$latest_id);
							exit();
						}else{
							$view = new \pongsit\view\view('message');
							$variables = array();
							$variables['message'] = 'มีบางอย่างผิดพลาด';
							echo $view->create($variables);
							error_log('Line signup failed: line/user-insert.php');
							exit();
						}
					}
				}else{
					header('Location: '.$path_to_core.'user/info.php?id='.$this_user_id);
					exit();
				}
			}else{
				$notification = $view->block('alert',array('message'=>'มีผู้ใช้ชื่อ '.$_POST['username'].' อยู่ในระบบแล้ว หากคุณคือเจ้าของกรุณา Log In หากไม่ใช่กรุณาเลือก Username ใหม่ครับ','type'=>'danger','css'=>'col'));
			}
		}else{
			$notification = $view->block('alert',array('message'=>'Password 2 ครั้งไม่ตรงกันครับ','type'=>'danger','css'=>'col-md-7'));
		}
	}
}else{
	$username = $displayName;
	$password = $_GET['userId'];
	if(!$user->exist($username)){
		$this_user_id = $line->get_user_id($line_id);
		if(!file_exists($path_to_root.'app/system/img/profile')){
			mkdir($path_to_root.'app/system/img/profile',0744,true);
		}
		if(empty($this_user_id)){
			$hash = $auth->generatePassword($password);
			$latest_id = $user->insert(array('name'=>$username,'password'=>$hash));
			$user_id = $user->get_last_inserted_id();
			$line->insert_user($line_id,$user_id);
			if(!(@$_SESSION['user']['id']==1 || $role->check('admin'))){
				if($line->login($_GET['userId'])){
					// $view->create(array('user_id'=>$latest_id));
					header('Location: '.$path_to_core.'user/info.php?id='.$latest_id);
					exit();
				}else{
					$view = new \pongsit\view\view('message');
					$variables = array();
					$variables['message'] = 'มีบางอย่างผิดพลาด';
					echo $view->create($variables);
					error_log('line signup failed: line/user-insert.php');
					exit();
				}
			}
		}else{
			header('Location: '.$path_to_core.'user/info.php?id='.$this_user_id);
			exit();
		}
	}else{
		$notification = $view->block('alert',array('message'=>'มีผู้ใช้ชื่อ '.$username.' อยู่ในระบบแล้ว หากคุณคือเจ้าของกรุณา Log In หากไม่ใช่กรุณาเลือก Username ใหม่ครับ','type'=>'danger','css'=>'col'));
	}
}

$variables['notification'] = $notification;
// $variables['h1']=$view->block('h1',array('message'=>'กรุณาสร้าง Username ที่ต้องการ','css'=>'col-md-7 text-center'));
// $role_ids = array();
// if(!empty($_SESSION['user']['id'])){
// 	$role_ids = $role->get_all_role_id($_SESSION['user']['id'],$_SESSION['team']['id']);
// }
// 
// $max_role = $role_ids[0]['role_id'];
// $skips = array();
// for($i=$max_role-1;$i>0;$i--){
// 	$skips[$i] = $i;
// }
// $variables['role_option'] = $option->from_db('role','last',$skips);
$variables['profile-image']=$line_user_infos['pictureUrl'];
$variables['profile-displayName']=$line_user_infos['displayName'];
$variables['header'] = $view->block('header');
$variables['page-name'] = 'สร้าง Username';
echo $view->create($variables);

