<?php
	
require_once("../system/init.php");

if(empty($confVariables['line'])){
	// require
	// $confVariables['line']['client_id']
	// $confVariables['line']['redirect_uri']
	// $confVariables['line']['state']
	// $confVariables['line']['secret']
	echo 'Please set up the config for line login. See the code for more detail.';
	exit();
}

if(!empty($_SESSION['access_token'])){
	// Get cURL resource
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPHEADER => array('Authorization: Bearer '.$_SESSION['access_token']),
		CURLOPT_URL => 'https://api.line.me/v2/profile'
	));
	
	// Send the request & save response to $resp
	$resp = curl_exec($curl);
	$outputs = (array) json_decode($resp);
	curl_close($curl);
	
	if(empty($outputs['userId'])){
		unset($_SESSION['access_token']);
		header("Refresh:0");
		exit();
	}
	
	$userId = $outputs['userId'];
	$auth->add_session('userId',$outputs['userId']);
	$auth->add_session('displayName',$outputs['displayName']);
	$auth->add_session('pictureUrl',$outputs['pictureUrl']);
	
	header('Location: '.$path_to_core.'line/user-info.php?userId='.$userId);

	
}else{
	if(!empty($_GET['code'])){

		// Get cURL resource
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_HTTPHEADER => array('Content-Type: application/x-www-form-urlencoded'),
		    CURLOPT_URL => 'https://api.line.me/oauth2/v2.1/token',
		    CURLOPT_POST => 1,
		    CURLOPT_POSTFIELDS => http_build_query(array(
		        "grant_type"=>"authorization_code",
		        "code"=>$_GET['code'],
				"redirect_uri"=>$confVariables['line']['redirect_uri'],
				"client_id"=>$confVariables['line']['client_id'],
				"client_secret"=>$confVariables['line']['secret'])
		    )
		));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		$outputs = json_decode($resp);
		
		if(!empty($outputs->access_token)){
			$_SESSION['access_token']=$outputs->access_token;
			header("Refresh:0");
		}
		
		// Close request to clear up some resources
		curl_close($curl);
	}else{
		header('Location: https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id='.$confVariables['line']['client_id'].'&redirect_uri='.$confVariables['line']['redirect_uri'].'&state='.$confVariables['line']['state'].'&scope=profile');
	}
}