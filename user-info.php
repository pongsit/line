<?php
	
require_once("../system/init.php");
$line = new \pongsit\line\line();
$role = new \pongsit\role\role();
$user = new \pongsit\user\user();

if(empty($_GET['userId'])){
	if(empty($_SESSION['access_token'])){
		if(!empty($confVariables['line']['client_id']) &&
		   !empty($confVariables['line']['redirect_uri']) &&
		   !empty($confVariables['line']['state'])
		){
			header('Location: https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id='.$confVariables['line']['client_id'].'&redirect_uri='.$confVariables['line']['redirect_uri'].'&state='.$confVariables['line']['state'].'&scope=profile');
			exit();
		}
	}
}

$userId = $_GET['userId'];
if(!($role->check('admin') || $role->check('staff') || $_SESSION['userId'] == $userId)){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

$line_user_infos = $line->get_the_info($userId);

if(empty($line->get_user_id($line_user_infos['id']))){
	$line_user_infos = $line->get_the_info($userId);
	$line_user_info_preps = array('userId'=>$userId,'displayName'=>$_SESSION['displayName'],'pictureUrl'=>$_SESSION['pictureUrl']);
	if(empty($line_user_infos)){
		$line->insert_once($line_user_info_preps,'userId',$userId);
	}else{
		$line->update($line_user_info_preps,' userId="'.$userId.'"');
	}
	header('Location: '.$path_to_core.'line/user-insert.php?userId='.$_GET['userId']);
	exit();
}

if($line->login($userId)){
	$redirect = 'index.php';
	if(!empty($_SESSION['user']['id'])){
		$redirect = $path_to_core.'user/info.php?id='.$_SESSION['user']['id'];
	}
	if(!empty($_GET['redirect'])){
		$redirect = $_GET['redirect'];
	}
	header('Location: '.$redirect);
	exit();
	
}else{
	$auth->logout();
	$view = new \pongsit\view\view('message');
	$variables = array();
	$variables['message'] = 'ไม่สามารถเข้าสู่ระบบได้ครับ';
	echo $view->create($variables);
	exit();
}

// if(!empty($_SESSION['goto'])){
// 	$goto = $_SESSION['goto'];
// 	unset($_SESSION['goto']);
// 	header('Location: '.$goto);
// }

// $variables['profile-image']=$line_user_infos['pictureUrl'];
// $variables['profile-displayName']=$line_user_infos['displayName'];
// 
// $info_list = '';
// $variables['edit_button']='';
// if(empty($info_list)){
// 	$variables['info_list']='<a href="#">กรุณาเพิ่มข้อมูลครับ</a> ';
// }else{
// 	$variables['edit_button']='<a href="#">แก้ไข</a>';
// }

// echo $view->create($variables);
